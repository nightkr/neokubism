pub trait Resource {
    type DynamicType;

    fn group(ty: &Self::DynamicType) -> &str;
    fn version(ty: &Self::DynamicType) -> &str;
    fn resource(ty: &Self::DynamicType) -> &str;

    fn api_prefix(ty: &Self::DynamicType) -> String {
        let version = Self::version(ty);
        match Self::group(ty) {
            "" => format!("api/{}/", version),
            group => format!("apis/{}/{}/", group, version),
        }
    }
}
impl<K: k8s_openapi::Resource> Resource for K {
    type DynamicType = ();

    fn group((): &Self::DynamicType) -> &str {
        K::GROUP
    }

    fn version((): &Self::DynamicType) -> &str {
        K::VERSION
    }

    fn resource((): &Self::DynamicType) -> &str {
        K::URL_PATH_SEGMENT
    }
}
pub struct DynamicObject;
pub struct DynamicType {
    group: String,
    version: String,
    kind: String,
    resource: String,
}
