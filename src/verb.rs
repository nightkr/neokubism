use futures::TryFuture;
use http::{Request, Response};
use hyper::Body;
use serde::{de::DeserializeOwned, Deserialize};
use snafu::{ResultExt, Snafu};

use crate::{
    decoder::{DecodeSingle, DecodeStream},
    resource::Resource,
    scope::{self, NativeScope},
    Config,
};

#[derive(Snafu, Debug)]
pub enum Error {
    #[snafu(display("verb created invalid http request: {}", source))]
    BuildRequestFailed { source: http::Error },
}
type Result<T, E = Error> = std::result::Result<T, E>;

pub trait Verb {
    type ResponseDecoder: TryFuture + From<Response<Body>>;

    fn to_http_request(&self, config: &Config) -> Result<Request<Body>>;
}

pub struct Get<Kind: Resource, Scope> {
    pub name: String,
    pub scope: Scope,
    pub dyn_type: Kind::DynamicType,
}
impl<Kind: Resource + DeserializeOwned, Scope: NativeScope<Kind>> Verb for Get<Kind, Scope> {
    type ResponseDecoder = DecodeSingle<Kind>;

    fn to_http_request(&self, config: &Config) -> Result<Request<Body>> {
        let version = Kind::version(&self.dyn_type);
        let api_prefix = match Kind::group(&self.dyn_type) {
            "" => format!("api/{}/", version),
            group => format!("apis/{}/{}/", group, version),
        };
        Request::get(format!(
            "{}{}{}{}/{}",
            config.base_url,
            api_prefix,
            self.scope.path_segment(),
            Kind::resource(&self.dyn_type),
            self.name
        ))
        .body(Body::empty())
        .context(BuildRequestFailed)
    }
}

pub struct List<Kind: Resource, Scope> {
    pub scope: Scope,
    pub dyn_type: Kind::DynamicType,
}
impl<Kind: Resource + DeserializeOwned, Scope: scope::Scope> Verb for List<Kind, Scope> {
    type ResponseDecoder = DecodeSingle<ObjectList<Kind>>;

    fn to_http_request(&self, config: &Config) -> Result<Request<Body>> {
        Request::get(format!(
            "{}{}{}{}",
            config.base_url,
            Kind::api_prefix(&self.dyn_type),
            self.scope.path_segment(),
            Kind::resource(&self.dyn_type),
        ))
        .body(Body::empty())
        .context(BuildRequestFailed)
    }
}
#[derive(Deserialize)]
pub struct ObjectList<Kind> {
    pub items: Vec<Kind>,
}

pub struct Watch<Kind: Resource, Scope> {
    pub scope: Scope,
    pub dyn_type: Kind::DynamicType,
}
impl<Kind: Resource, Scope: scope::Scope> Verb for Watch<Kind, Scope> {
    type ResponseDecoder = DecodeStream<WatchEvent<Kind>>;

    fn to_http_request(&self, config: &Config) -> Result<Request<Body>> {
        Request::get(format!(
            "{}{}{}{}?watch=1",
            config.base_url,
            Kind::api_prefix(&self.dyn_type),
            self.scope.path_segment(),
            Kind::resource(&self.dyn_type),
        ))
        .body(Body::empty())
        .context(BuildRequestFailed)
    }
}
#[derive(Deserialize, Debug)]
#[serde(tag = "type", content = "object", rename_all = "UPPERCASE")]
pub enum WatchEvent<Kind> {
    Added(Kind),
    Deleted(Kind),
    Modified(Kind),
    Bookmark(Bookmark),
}
#[derive(Deserialize, Debug)]
pub struct Bookmark {
    pub metadata: BookmarkMeta,
}
#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct BookmarkMeta {
    pub resource_version: String,
}
