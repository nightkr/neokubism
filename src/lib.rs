#![deny(clippy::unwrap_used)]

pub mod client;
pub mod decoder;
pub mod resource;
pub mod scope;
pub mod verb;

pub struct Config {
    pub base_url: String,
}
