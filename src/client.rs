use crate::{
    verb::{self, Verb},
    Config,
};
use futures::{TryFuture, TryFutureExt};
use hyper::client::HttpConnector;
use snafu::{ResultExt, Snafu};

#[derive(Snafu, Debug)]
pub enum Error<DecodeErr: std::error::Error + 'static> {
    #[snafu(display("failed to build api request: {}", source))]
    BuildRequestFailed { source: verb::Error },
    #[snafu(display("kube api request failed: {}", source))]
    RequestFailed { source: hyper::Error },
    #[snafu(display("failed to decode api response: {}", source))]
    DecodeFailed { source: DecodeErr },
}

pub struct Client {
    http: hyper::Client<HttpConnector>,
    config: Config,
}

impl Client {
    pub fn new(config: Config) -> Self {
        Self {
            http: hyper::Client::new(),
            config,
        }
    }

    pub async fn call<V: Verb>(
        &self,
        verb: V,
    ) -> Result<
        <V::ResponseDecoder as TryFuture>::Ok,
        Error<<V::ResponseDecoder as TryFuture>::Error>,
    >
    where
        <V::ResponseDecoder as TryFuture>::Error: std::error::Error + 'static,
    {
        let req = verb
            .to_http_request(&self.config)
            .context(BuildRequestFailed)?;
        tracing::debug!(verb = %req.method(), uri = %req.uri(), "executing request");
        V::ResponseDecoder::from(self.http.request(req).await.context(RequestFailed)?)
            .into_future()
            .await
            .context(DecodeFailed)
    }
}
