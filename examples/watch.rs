use futures::StreamExt;
use k8s_openapi::api::core::v1::Pod;
use neokubism::{client::Client, scope::ClusterScope, verb::Watch, Config};
use tracing_subscriber::EnvFilter;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .init();
    let client = Client::new(Config {
        // run kubectl proxy
        base_url: "http://localhost:8001/".to_string(),
    });
    client
        .call(Watch::<Pod, _> {
            scope: ClusterScope,
            dyn_type: (),
        })
        .await
        .unwrap()
        .for_each(|item| async move {
            dbg!(item.unwrap());
        })
        .await;
}
