use k8s_openapi::api::core::v1::{Namespace, Pod};
use neokubism::{
    client::Client,
    scope::{ClusterScope, NamespaceScope},
    verb::List,
    Config,
};
use tracing_subscriber::EnvFilter;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .init();
    let client = Client::new(Config {
        // run kubectl proxy
        base_url: "http://localhost:8001/".to_string(),
    });
    for ns in client
        .call(List::<Namespace, _> {
            scope: ClusterScope,
            dyn_type: (),
        })
        .await
        .unwrap()
        .items
    {
        println!("Pods in {}:", ns.metadata.name.clone().unwrap());
        for pod in client
            .call(List::<Pod, _> {
                scope: NamespaceScope {
                    namespace: ns.metadata.name.clone().unwrap(),
                },
                dyn_type: (),
            })
            .await
            .unwrap()
            .items
        {
            println!("- {}", pod.metadata.name.unwrap());
        }
    }
}
